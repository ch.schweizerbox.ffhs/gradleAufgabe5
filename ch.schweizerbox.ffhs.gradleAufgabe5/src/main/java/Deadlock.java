
/**
 * Aufgabe: Dieses Programm demonstriert einen Deadlock. Lassen Sie dieses
 * Programm mehrfach laufen und schauen Sie, was passiert.
 * 
 * a) Erklären Sie das Verhalten des Programms. b) Korrigieren Sie das
 * Programm, so dass es sich korrekt verhält. Verändern Sie dabei nicht die
 * Klasse Friend.
 * 
 * @author bele
 * 
 */
public class Deadlock {

	void doStuff() throws InterruptedException {
		final Friend alphonse = new Friend("Alphonse");
		final Friend gaston = new Friend("Gaston");
		final SimpleSemaphore ss = new SimpleSemaphore(1);
		Thread gastonThread = new Thread(new Runnable() {
			public void run() {
				try {
					ss.acquire();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				alphonse.bow(gaston);
				ss.release();
			}
		}, "Gaston");

		gastonThread.start();

		Thread alphonseThread = new Thread(new Runnable() {
			public void run() {
				try {
					ss.acquire();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				gaston.bow(alphonse);
				ss.release();
			}
		}, "Alphonse");

		alphonseThread.start();
		Thread.sleep(3000);

		System.out.println("lebt A? " + alphonseThread.isAlive());
		System.out.println("lebt G? " + gastonThread.isAlive());

	}

	public static void main(String[] args) throws InterruptedException {
		Deadlock d = new Deadlock();
		d.doStuff();
	}
}

class Friend {
	private final String name;

	public Friend(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public synchronized void bow(Friend bower) {
		System.out.format("%s: %s" + "  has bowed to me!%n", this.name, bower.getName());
		bower.bowBack(this);
	}

	public synchronized void bowBack(Friend bower) {
		System.out.format("%s: %s" + " has bowed back to me!%n", this.name, bower.getName());
	}
}

final class SimpleSemaphore {
	private int count;

	public SimpleSemaphore(final int n) {
		this.count = n;
	}

	public synchronized void acquire() throws InterruptedException {
		waitWhileNoResources();
		// System.out.println(Thread.currentThread().getName() + "
		// geschnappt!");
		count--;
	}

	public synchronized void release() {
		count++;
		// System.out.println(Thread.currentThread().getName() + " released!");
		notifyAll();
	}

	private void waitWhileNoResources() throws InterruptedException {
		while (count == 0) {
			wait();
			// System.out.println(Thread.currentThread().getName() + "
			// warten....");
		}

	}
}
